# Build MPC Solver


The file mpc_solver_setup.cpp is what defines the MPC controller and it is this you should change in order to change the controller. Then build/install it using the explanation bellow.

Prerequests:

ACADO toolkit needs to be properly installed. Follow the instructions here: http://acado.github.io/install_linux.html

	source [AcadoPath]/build/acado_env.sh

Build/Install:

From this folder do:

	mkdir build 
	cd build 
	cmake .. 
	make

The executable is now placed in the previous folder
	
	cd .. 

Execute it by running the following commands:

	./mpc_solver_setup

Then the controller is generated.
