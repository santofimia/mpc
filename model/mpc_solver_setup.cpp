#include <acado_code_generation.hpp>

int main( )
{
  USING_NAMESPACE_ACADO


    // INTRODUCE THE VARIABLES:
    // -------------------------
    // Take care with declaration order, maintain the same order in H and Q
    DifferentialState x, y , z; // Position
    DifferentialState yaw; // Attitude World coordinates
  	DifferentialState v_x, v_y, v_z, dyaw; // Speed
    DifferentialState a_x, a_y, a_z, a_yaw; // Aceleration

    // To be able to derive controls
    DifferentialState pitch_b, roll_b, dyaw_cmd, dz; // Inputs on autopilot 
  	Control dpitch_b, droll_b, ddyaw_cmd, ddz; 

  IntermediateState pitch_w, roll_w, dpitch_w, droll_w;

  DifferentialEquation f;

  Function h, hN;

	OnlineData A_pr, resp_time_pr, delay_pr;
	OnlineData A_dz, resp_time_dz, delay_dz;
	OnlineData A_dy, resp_time_dy, delay_dy;

  // Parameters with exemplary values. These are set/overwritten at runtime.
  const double t_start = 0.0;     // Initial time [s]
  const double t_end = 2.0;       // Time horizon [s]
  const double dt = 0.1;          // Discretization time [s]
  const int N = round(t_end/dt);  // Number of nodes
  const double roll_pitch_max = 0.5;     // Max pitch and roll [rad]
  const double dyaw_max = 2.0;      // Max yaw rate  [rad/s]
  const double dz_max = 2.0;         // Max altitude rate [m/s]

  // Model Parameters

//  const double A_pr = 27.10; // Transfer Function numerator from pitch/roll
//  const double resp_time_pr = 4.5; // Transfer Function time response from pitch/roll
//  const double delay_pr = 0.171; //Transfer Funtion delay from pitch/roll
  const double sat_pr = 20; // Transfer Function saturation from pitch/roll

//  const double A_dz = 0.5637; // Transfer Function numerator from dAltitude
//  const double resp_time_dz = 0.2409; // Transfer Function time response from dAltitude
//  const double delay_dz = 0.041; //Transfer Funtion delay from dAltitude
  const double sat_dz = 1.0; // Transfer Function saturation from dAltitude

//  const double A_dy = -1.786; // Transfer Function numerator from dYaw
//  const double resp_time_dy = 0.3824; // Transfer Function time response from dYaw
//  const double delay_dy = 0.075; //Transfer Funtion delay from dYaw
  const double sat_dy = 1.786; // Transfer Function saturation from dYaw

    // DEFINE A DIFFERENTIAL EQUATION:
    // -------------------------------

  pitch_w = pitch_b * cos(yaw) - roll_b * sin(yaw);
  roll_w =  - pitch_b * sin(yaw) - roll_b * cos(yaw);

  dpitch_w = dpitch_b * cos (yaw) - pitch_b * sin(yaw) * dyaw - droll_b * sin(yaw) - roll_b * cos(yaw) * dyaw;
  droll_w = - dpitch_b * sin(yaw) - pitch_b * cos(yaw) * dyaw - droll_b * cos(yaw) + roll_b * sin(yaw) * dyaw;

  f << dot(x) == v_x;
  f << dot(y) == v_y;
  f << dot(z) == v_z;
  f << dot(yaw) == dyaw;
	//
  f << dot(v_x) == a_x;
  f << dot(v_y) == a_y;
  f << dot(v_z) == a_z;
  f << dot(dyaw) == a_yaw;
	//
  f << dot(a_x) == - (-A_pr)/resp_time_pr * dpitch_w + 2*(-A_pr)/(delay_pr*resp_time_pr) * pitch_w - (resp_time_pr+delay_pr/2)*delay_pr*resp_time_pr/(2) * a_x - 2/(resp_time_pr*delay_pr)*v_x;
  f << dot(a_y) == -   A_pr/resp_time_pr * droll_w +   2*A_pr/(delay_pr*resp_time_pr) * roll_w -     (resp_time_pr+delay_pr/2)*delay_pr*resp_time_pr/(2) * a_y - 2/(resp_time_pr*delay_pr)*v_y;
  f << dot(a_z) == -   A_dz/resp_time_dz * ddz +       2*A_dz/(delay_dz*resp_time_dz) * dz -         (resp_time_dz+delay_dz/2)*delay_dz*resp_time_dz/(2) * a_z - 2/(resp_time_dz*delay_dz)*v_z;
  f << dot(a_yaw) == - A_dy/resp_time_dy * ddyaw_cmd + 2*A_dy/(delay_dy*resp_time_dy) * dyaw_cmd -   (resp_time_dy+delay_dy/2)*delay_dy*resp_time_dy/(2) * a_yaw - 2/(resp_time_dy*delay_dy)*dyaw;
  //
  f << dot(pitch_b) == dpitch_b;
  f << dot(roll_b) == droll_b;
  f << dot(dyaw_cmd) == ddyaw_cmd;
  f << dot(dz) == ddz;


    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    // Cost: Sum(i=0, ..., N-1){h_i' * Q * h_i} + h_N' * Q_N * h_N
    // Running cost vector consists of all states and inputs.
  h << x << y << z << yaw
    << v_x << v_y << v_z << dyaw
    << pitch_b << roll_b << dyaw_cmd << dz
    << dpitch_b << droll_b << ddyaw_cmd << ddz;

  // End cost vector consists of all states (no inputs at last state).
  hN << x << y << z << yaw
    << v_x << v_y << v_z << dyaw;

  BMatrix W = eye<bool>( h.getDim() );;
  BMatrix WN = eye<bool>(hN.getDim());


  OCP ocp( t_start, t_end, N );

  ocp.subjectTo( f );


  ocp.minimizeLSQ( W, h);
  ocp.minimizeLSQEndTerm( WN, hN );

  // ocp.subjectTo( -3.14 <= yaw <= 3.14 );
  ocp.subjectTo( - roll_pitch_max <= pitch_b <= roll_pitch_max );
  ocp.subjectTo( - roll_pitch_max <= roll_b <= roll_pitch_max );
  ocp.subjectTo( - dyaw_max <= dyaw_cmd <= dyaw_max );
  ocp.subjectTo( - dz_max <= dz <= dz_max );
  ocp.subjectTo( - sat_pr <= v_x <= sat_pr );
  ocp.subjectTo( - sat_pr <= v_y <= sat_pr );
  ocp.subjectTo( - sat_dz <= v_z <= sat_dz );
  ocp.subjectTo( - sat_dy <= dyaw <= sat_dy );
  ocp.subjectTo( 0.1 <= z );

  ocp.setNOD(9);




  OCPexport mpc(ocp);

  mpc.set(HESSIAN_APPROXIMATION,  GAUSS_NEWTON);        // is robust, stable
  mpc.set(DISCRETIZATION_TYPE,    MULTIPLE_SHOOTING);   // good convergence
  mpc.set(SPARSE_QP_SOLUTION,     FULL_CONDENSING_N2);  // due to qpOASES
  mpc.set(INTEGRATOR_TYPE,        INT_IRK_GL4);         // accurate
  mpc.set(NUM_INTEGRATOR_STEPS,   N);
  mpc.set(QP_SOLVER,              QP_QPOASES);          // free, source code
  mpc.set(HOTSTART_QP,            YES);
  mpc.set(CG_USE_OPENMP,                    YES);       // paralellization
  mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,    NO);        // set on runtime
  mpc.set(CG_USE_VARIABLE_WEIGHTING_MATRIX, NO);       // time-varying costs
  mpc.set(USE_SINGLE_PRECISION,        YES);           // Single precision

  // Do not generate tests, makes or matlab-related interfaces.
  mpc.set( GENERATE_TEST_FILE,          NO);
  mpc.set( GENERATE_MAKE_FILE,          NO);
  mpc.set( GENERATE_MATLAB_INTERFACE,   NO);
  mpc.set( GENERATE_SIMULINK_INTERFACE, NO);

    // Finally, export everything.
    if(mpc.exportCode("mpc_codegen") != SUCCESSFUL_RETURN)
      exit( EXIT_FAILURE );
    mpc.printDimensionsQP( );


  return EXIT_SUCCESS;

}
