#ifndef mpc_process
#define mpc_process

#include "robot_process.h"

/*!*************************************************************************************
 *  \class     mpc_process
 *
 *  \brief     MPC Controller
 *
 *  \details   This process receives a altitude message through an input topic.  (Cambiar)
 *             If either the x or y values are negative, an error is reported.
 *             This process uses algorithms defined by the MyClass class.
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "eigen3/Eigen/Eigen"

#include "xmlfilereader.h"

#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "control/simpletrajectorywaypoint.h"
//Services
#include "droneMsgsROS/setControlMode.h"


#include "acado_common.h"
#include "acado_auxiliary_functions.h"

#include <stdio.h>

/* Some convenient definitions. */
#define NX          ACADO_NX  /* Number of differential state variables.  */
//#define NXA         ACADO_NXA /* Number of algebraic variables. */
#define NU          ACADO_NU  /* Number of control inputs. */
#define NOD         ACADO_NOD  /* Number of online data values. */

#define NY          ACADO_NY  /* Number of measurements/references on nodes 0..N - 1. */
#define NYN         ACADO_NYN /* Number of measurements/references on node N. */

#define N           ACADO_N   /* Number of intervals in the horizon. */

//#define NUM_STEPS   10      /* Number of real-time iterations. */
//#define VERBOSE     1         /* Show iterations: 1, silent: 0.  */

class MPCProcess: public RobotProcess
{
public:

/*!*************************************************************************************
 * \details The function ownSetUp() is executed to setup everything that
 *          the process needs at the beginning such as read
 *          configuration files, read ROS parameters, etc.
 **************************************************************************************/
  void ownSetUp();

/*!*************************************************************************************
 * \details The function ownStart() prepares the process to start running.
 *          Connects topics and services and set the appropiate values.
 **************************************************************************************/
  void ownStart();

/*!*************************************************************************************
 * \details The function ownStop() perform shutdown of topics and services and
 *          reset values of variables.
 **************************************************************************************/
  void ownStop();

/*!*************************************************************************************
 * \details The function ownRun() the main body of execution to perform in the
 *          running state. Only used when the process is synchronous.
 **************************************************************************************/
  void ownRun();

  void getOutput();
  void setReference();
  void setFeedback();

  void setModel();
  void setCost();

  //! Read Config
  bool readConfigs(std::string configFile);
  void readParameters();

  //! Constructor. \details Same arguments as the ros::init function.
  MPCProcess();

  //! Destructor.
  ~MPCProcess();

private:

  //! ROS NodeHandler used to manage the ROS communication
  ros::NodeHandle n;
  //! ROS subscriber handler used to receive messages.
  ros::Subscriber pos_sub;

  //! Topic name used to subscribe
  std::string pos_topic;

  /* Some temporary variables. */
  int i, j;
//  acado_timer t;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string mpcconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name

  // [Subscribers] Controller references
private:

    std::string dronePositionRefTopicName;
    ros::Subscriber dronePositionRefSub;
    void dronePositionRefsSubCallback(const droneMsgsROS::dronePositionRefCommandStamped::ConstPtr &msg);

    std::string droneSpeedsRefTopicName;
    ros::Subscriber droneSpeedsRefSub;
    void droneSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);

    std::string droneTrajectoryAbsRefTopicName;
    ros::Subscriber droneTrajectoryAbsRefSub;  // Trajectory specified in world coordinates
    void droneTrajectoryAbsRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);

    std::string droneTrajectoryRelRefTopicName;
    ros::Subscriber droneTrajectoryRelRefSub;  // Trajectory specified relative to current drone pose
    void droneTrajectoryRelRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);
    bool stay_in_position;

    std::string droneYawRefCommandTopicName;
    ros::Subscriber droneYawRefCommandSub;
    void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);
    droneMsgsROS::droneYawRefCommand current_yaw_reference;

    // [Publishers] Controller references, meaning of useful values depend on the current control mode
    // useful values: x, y, z, yaw
    std::string drone_position_reference_topic_name;
    ros::Publisher drone_position_reference_publisher;
    droneMsgsROS::dronePositionRefCommand current_drone_position_reference;

    ros::Publisher drone_yaw_reference_publisher;

    // useful values: vxfi, vyfi, vzfi (dyawfi is unused, undebugged)
    std::string drone_speed_reference_topic_name;
    ros::Publisher drone_speed_reference_publisher;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;

    // useful values: current trajectory: initial_checkpoint,isPeridioc, checkpoints[]
    std::string drone_trajectory_reference_topic_name;
    ros::Publisher drone_trajectory_reference_publisher;
    droneMsgsROS::dronePositionTrajectoryRefCommand current_drone_trajectory_command;

    std::vector<SimpleTrajectoryWaypoint> waypoints;
    droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_command;
    droneMsgsROS::dronePositionTrajectoryRefCommand actual_trajectory;
    bool traj_end;
    droneMsgsROS::dronePositionTrajectoryRefCommand rel_trajectory_command;

    // [Subscribers] State estimation: controller feedback inputs
private:

    std::string droneEstimatedPoseTopicName;
    ros::Subscriber droneEstimatedPoseSubs;
    void droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    droneMsgsROS::dronePose last_estimatedPose;

    std::string droneEstimatedSpeedsTopicName;
    ros::Subscriber droneEstimatedSpeedsSubs;
    void droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr& msg);
    droneMsgsROS::droneSpeeds last_estimatedSpeed;

    ros::Subscriber droneControlModeSubs;
    void droneControlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode::ConstPtr& msg);
    std::string controlModeTopicName;
    droneMsgsROS::droneTrajectoryControllerControlMode control_mode;

    // Publishers
private:

    std::string drone_command_pitch_roll_topic_name;
    ros::Publisher drone_command_pitch_roll_publisher;
    droneMsgsROS::dronePitchRollCmd drone_command_pitch_roll_msg;

    std::string drone_command_dyaw_topic_name;
    ros::Publisher drone_command_dyaw_publisher;
    droneMsgsROS::droneDYawCmd drone_command_dyaw_msg;

    std::string drone_command_daltitude_topic_name;
    ros::Publisher drone_command_daltitude_publisher;
    droneMsgsROS::droneDAltitudeCmd drone_command_daltitude_msg;

//  // Control Timing
//  double t_hor;
    double dt = 0.1;
    double t;
//  const int N = round(t_end/dt);  // Number of nodes

    //Trajectory
    droneMsgsROS::dronePositionRefCommand last_point;
    droneMsgsROS::dronePositionRefCommand actual_point;
    droneMsgsROS::dronePositionRefCommand next_point;
    float number_point;
    int iterator;

  // Model
  double sat_pr, sat_dyaw, sat_dz; // Model saturations
  double A_pr, A_dz, A_dyaw; // Model parameters (Gain)
  double t_r_pr, t_r_dz, t_r_dyaw; // Model parameters (time response)
  double delay_pr, delay_dz, delay_dyaw; // Model parameters (Delay)


  // Commands Saturation
  double sat_cmd_pr, sat_cmd_dyaw, sat_cmd_dz;

  // Cost function weights
  double q_pos, q_z, q_yaw;
  double q_vel, q_vz, q_dyaw;
  double r_pr, r_dz, r_dyaw;

  // Safety minimun height
  double z_min;
  // Hover height
  double z_hover;

  double error_yaw;
  double ref_yaw;

  // Trajectory calculation
  Eigen::Vector3d point1, point2, point, point_actual;
  double vmax_xy, vmax_z;
  double v_xy, v_z;

  double dist_xy, dist_z, sgndist_z;
  double  thr_xy, thr_z;

  int iter_point, iter_move, it;
  bool endpoint;
  double last_dist, dist;

  double max(double d1,double d2);

};
#endif 
