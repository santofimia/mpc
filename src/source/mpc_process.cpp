#include "mpc_process.h"


/* Global variables used by the solver. */
// Have to declare on cpp (extern on acado_common.h)
ACADOvariables acadoVariables;
ACADOworkspace acadoWorkspace;

//Constructor
MPCProcess::MPCProcess():RobotProcess()
{
  current_yaw_reference.yaw = 0.0;
  current_drone_position_reference.x = 0.0;
  current_drone_position_reference.y = 0.0;
  current_drone_position_reference.z = 1.0;
  current_drone_speed_reference.dx = 0.0;
  current_drone_speed_reference.dy = 0.0;
  current_drone_speed_reference.dz = 0.0;
  current_drone_speed_reference.dyaw = 0.0;
  current_drone_trajectory_command.droneTrajectory.clear();
  trajectory_command.droneTrajectory.clear();
  actual_trajectory.droneTrajectory.clear();
  rel_trajectory_command.droneTrajectory.clear();
  last_estimatedPose.x = 0.0;
  last_estimatedPose.y = 0.0;
  last_estimatedPose.z = 1.0;
  last_estimatedPose.yaw = 0.0;
  last_estimatedSpeed.dx = 0.0;
  last_estimatedSpeed.dy = 0.0;
  last_estimatedSpeed.dz = 0.0;
  last_estimatedSpeed.dyaw = 0.0;
  control_mode.command = Controller_MidLevel_controlMode::POSITION_CONTROL;
  std::cout << "Constructor: MPC" << std::endl;

}

//Destructor
MPCProcess::~MPCProcess()
{
  std::cout << "Destructor: MPC" << std::endl;

}

bool MPCProcess::readConfigs(std::string configFile)
{
  try
  {

  XMLFileReader my_xml_reader(configFile);

  // Model parameters
  sat_pr = my_xml_reader.readDoubleValue("MPC:Model:Saturation:pitch_roll");
  sat_dyaw = my_xml_reader.readDoubleValue("MPC:Model:Saturation:dyaw");
  sat_dz = my_xml_reader.readDoubleValue("MPC:Model:Saturation:dz");
  A_pr = my_xml_reader.readDoubleValue("MPC:Model:Gain:pitch_roll");
  A_dz = my_xml_reader.readDoubleValue("MPC:Model:Gain:dz");
  A_dyaw = my_xml_reader.readDoubleValue("MPC:Model:Gain:dyaw");
  t_r_pr = my_xml_reader.readDoubleValue("MPC:Model:Time_response:pitch_roll");
  t_r_dz = my_xml_reader.readDoubleValue("MPC:Model:Time_response:dz");
  t_r_dyaw = my_xml_reader.readDoubleValue("MPC:Model:Time_response:dyaw");
  delay_pr = my_xml_reader.readDoubleValue("MPC:Model:Delay:pitch_roll");
  delay_dz = my_xml_reader.readDoubleValue("MPC:Model:Delay:dz");
  delay_dyaw = my_xml_reader.readDoubleValue("MPC:Model:Delay:dyaw");

  // Command limits
  sat_cmd_pr = my_xml_reader.readDoubleValue("MPC:Cmd_limits:pitch_roll");
  sat_cmd_dyaw = my_xml_reader.readDoubleValue("MPC:Cmd_limits:dyaw");
  sat_cmd_dz = my_xml_reader.readDoubleValue("MPC:Cmd_limits:dz");

  // Cost function
  q_pos = my_xml_reader.readDoubleValue("MPC:Cost:State:position_xy");
  q_z = my_xml_reader.readDoubleValue("MPC:Cost:State:position_z");
  q_yaw = my_xml_reader.readDoubleValue("MPC:Cost:State:yaw");
  q_vel = my_xml_reader.readDoubleValue("MPC:Cost:State:speed_xy");
  q_vz = my_xml_reader.readDoubleValue("MPC:Cost:State:speed_z");
  q_dyaw = my_xml_reader.readDoubleValue("MPC:Cost:State:dyaw");
  r_pr = my_xml_reader.readDoubleValue("MPC:Cost:Command:pitch_roll");
  r_dz = my_xml_reader.readDoubleValue("MPC:Cost:Command:dz");
  r_dyaw = my_xml_reader.readDoubleValue("MPC:Cost:Command:dyaw");

  // Minimum and hover Height
  z_min = my_xml_reader.readDoubleValue("MPC:Height:minimun");
  z_hover = my_xml_reader.readDoubleValue("MPC:Height:hover");

  // Trajcetory speed
  vmax_xy = my_xml_reader.readDoubleValue("MPC:Trajectory:Speed_xy");
  vmax_z = my_xml_reader.readDoubleValue("MPC:Trajectory:Speed_z");;

  }

  catch ( cvg_XMLFileReader_exception &e)
  {
    throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
  }

  return true;
}

void MPCProcess::readParameters()
{
  // Configs
  //
  ros::param::get("~stackPath", stackPath);
  if ( stackPath.length() == 0)
  {
      stackPath = "$(env AEROSTACK_STACK)";
  }
  std::cout<<"stackPath="<<stackPath<<std::endl;
  //
  ros::param::get("~droneId", idDrone);
  std::cout<<"droneId="<<idDrone<<std::endl;
  //
  ros::param::get("~mpc_config_file", mpcconfigFile);
  if ( mpcconfigFile.length() == 0)
  {
      mpcconfigFile="mpc_controller.xml";
  }
  std::cout<<"config_file="<<mpcconfigFile<<std::endl;
  // Topics
  //
  ros::param::get("~drone_position_ref_topic_name", dronePositionRefTopicName);
  if ( dronePositionRefTopicName.length() == 0)
  {
      dronePositionRefTopicName="dronePositionRefs";
  }
  std::cout<<"drone_position_ref_topic_name="<<dronePositionRefTopicName<<std::endl;
  //
  ros::param::get("~drone_speeds_ref_topic_name", droneSpeedsRefTopicName);
  if ( droneSpeedsRefTopicName.length() == 0)
  {
      droneSpeedsRefTopicName="droneSpeedsRefs";
  }
  std::cout<<"drone_speeds_ref_topic_name="<<droneSpeedsRefTopicName<<std::endl;
  //
  ros::param::get("~drone_trajectory_abs_ref_topic_name", droneTrajectoryAbsRefTopicName);
  if ( droneTrajectoryAbsRefTopicName.length() == 0)
  {
      droneTrajectoryAbsRefTopicName="droneTrajectoryAbsRefCommand";
  }
  std::cout<<"drone_trajectory_abs_ref_topic_name="<<droneTrajectoryAbsRefTopicName<<std::endl;
  //
  ros::param::get("~drone_trajectory_rel_ref_topic_name", droneTrajectoryRelRefTopicName);
  if ( droneTrajectoryRelRefTopicName.length() == 0)
  {
      droneTrajectoryRelRefTopicName="droneTrajectoryRefCommand";
  }
  std::cout<<"drone_trajectory_rel_ref_topic_name="<<droneTrajectoryRelRefTopicName<<std::endl;
  //
  ros::param::get("~drone_yaw_ref_command_topic_name", droneYawRefCommandTopicName);
  if ( droneYawRefCommandTopicName.length() == 0)
  {
      droneYawRefCommandTopicName="droneControllerYawRefCommand";
  }
  std::cout<<"drone_yaw_ref_command_topic_name="<<droneYawRefCommandTopicName<<std::endl;
  //
  ros::param::get("~drone_position_reference_topic_name", drone_position_reference_topic_name);
  if ( drone_position_reference_topic_name.length() == 0)
  {
      drone_position_reference_topic_name="trajectoryControllerPositionReferencesRebroadcast";
  }
  std::cout<<"drone_position_reference_topic_name="<<drone_position_reference_topic_name<<std::endl;
  //
  ros::param::get("~drone_speed_reference_topic_name", drone_speed_reference_topic_name);
  if ( drone_speed_reference_topic_name.length() == 0)
  {
      drone_speed_reference_topic_name="trajectoryControllerSpeedReferencesRebroadcast";
  }
  std::cout<<"drone_speed_reference_topic_name="<<drone_speed_reference_topic_name<<std::endl;
  //
  ros::param::get("~drone_trajectory_reference_topic_name", drone_trajectory_reference_topic_name);
  if ( drone_trajectory_reference_topic_name.length() == 0)
  {
      drone_trajectory_reference_topic_name="trajectoryControllerTrajectoryReferencesRebroadcast";
  }
  std::cout<<"drone_trajectory_reference_topic_name="<<drone_trajectory_reference_topic_name<<std::endl;
  //
  ros::param::get("~drone_estimated_pose_topic_name", droneEstimatedPoseTopicName);
  if ( droneEstimatedPoseTopicName.length() == 0)
  {
      droneEstimatedPoseTopicName="EstimatedPose_droneGMR_wrt_GFF";
  }
  std::cout<<"drone_estimated_pose_topic_name="<<droneEstimatedPoseTopicName<<std::endl;
  //
  ros::param::get("~drone_estimated_speeds_topic_name", droneEstimatedSpeedsTopicName);
  if ( droneEstimatedSpeedsTopicName.length() == 0)
  {
      droneEstimatedSpeedsTopicName="EstimatedSpeed_droneGMR_wrt_GFF";
  }
  std::cout<<"drone_estimated_speeds_topic_name="<<droneEstimatedSpeedsTopicName<<std::endl;
  //
  ros::param::get("~control_mode_topic_name", controlModeTopicName);
  if ( controlModeTopicName.length() == 0)
  {
      controlModeTopicName="flight_motion_controller_process/controlMode";
  }
  std::cout<<"control_mode_topic_name="<<controlModeTopicName<<std::endl;
  //
  ros::param::get("~drone_command_pitch_roll_topic_name", drone_command_pitch_roll_topic_name);
  if ( drone_command_pitch_roll_topic_name.length() == 0)
  {
      drone_command_pitch_roll_topic_name="command/pitch_roll";
  }
  std::cout<<"drone_command_pitch_roll_topic_name="<<drone_command_pitch_roll_topic_name<<std::endl;
  //
  ros::param::get("~drone_command_dyaw_topic_name", drone_command_dyaw_topic_name);
  if ( drone_command_dyaw_topic_name.length() == 0)
  {
      drone_command_dyaw_topic_name="command/dYaw";
  }
  std::cout<<"drone_command_dyaw_topic_name="<<drone_command_dyaw_topic_name<<std::endl;
  //
  ros::param::get("~drone_command_daltitude_topic_name", drone_command_daltitude_topic_name);
  if ( drone_command_daltitude_topic_name.length() == 0)
  {
      drone_command_daltitude_topic_name="command/dAltitude";
  }
  std::cout<<"drone_command_daltitude_topic_name="<<drone_command_daltitude_topic_name<<std::endl;

  return;
}

//ownSetUp()
void MPCProcess::ownSetUp()
{

  // Parameters
  readParameters();

  bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+mpcconfigFile);

  if(!readConfigsBool)
  {
      std::cout << "Error init"<< std::endl;
      return;
  }

  /* Clear solver memory. */
  memset(&acadoWorkspace, 0, sizeof( acadoWorkspace ));
  memset(&acadoVariables, 0, sizeof( acadoVariables ));

  /* Initialize Weights */
  for (i = 0; i < N * NY; i++)
  {
    for (j = 0; j < NY; j++)
    {
      if (i % NY == j)
      {
        acadoVariables.W[i * NY + j] = 1;
      }
    }
  }

  for (i = 0; i < NYN; i++)
  {
    acadoVariables.WN[i + NYN * i] = 1;
  }


  setModel();
  setCost();

  // Initialize the solver.
  acado_initializeSolver();

  // Initialize the states and controls.
  for (i = 0; i < (N + 1); ++i)
    {
    acadoVariables.x[ i * NX + 0 ] = last_estimatedPose.x;
    acadoVariables.x[ i * NX + 1 ] = last_estimatedPose.y;
    acadoVariables.x[ i * NX + 2 ] = last_estimatedPose.z;
    acadoVariables.x[ i * NX + 3 ] = last_estimatedPose.yaw;
    acadoVariables.x[ i * NX + 4 ] = 0.0;
    acadoVariables.x[ i * NX + 5 ] = 0.0;
    acadoVariables.x[ i * NX + 6 ] = 0.0;
    acadoVariables.x[ i * NX + 7 ] = 0.0;
    acadoVariables.x[ i * NX + 8 ] = 0.0;
    acadoVariables.x[ i * NX + 9 ] = 0.0;
    acadoVariables.x[ i * NX + 10 ] = 0.0;
    acadoVariables.x[ i * NX + 12 ] = 0.0;
    acadoVariables.x[ i * NX + 13 ] = 0.0;
    acadoVariables.x[ i * NX + 14 ] = 0.0;
    acadoVariables.x[ i * NX + 15 ] = 0.0;

  }

  for (i = 0; i < NU * N; ++i)
    acadoVariables.u[i] = 0.0;

  for (i = 0; i < N; ++i)
    {
    acadoVariables.y[i * NY + 0] = last_estimatedPose.x;
    acadoVariables.y[i * NY + 1] = last_estimatedPose.y;
    acadoVariables.y[i * NY + 2] = last_estimatedPose.z;
    acadoVariables.y[i * NY + 3] = last_estimatedPose.yaw;
    acadoVariables.y[i * NY + 4] = 0.0;
    acadoVariables.y[i * NY + 5] = 0.0;
    acadoVariables.y[i * NY + 6] = 0.0;
    acadoVariables.y[i * NY + 7] = 0.0;
    acadoVariables.y[i * NY + 8] = 0.0;
    acadoVariables.y[i * NY + 9] = 0.0;
    acadoVariables.y[i * NY + 10] = 0.0;
    acadoVariables.y[i * NY + 11] = 0.0;
    acadoVariables.y[i * NY + 12] = 0.0;
    acadoVariables.y[i * NY + 13] = 0.0;
    acadoVariables.y[i * NY + 14] = 0.0;
    acadoVariables.y[i * NY + 15] = 0.0;


  }

  acadoVariables.yN[0] = last_estimatedPose.x;
  acadoVariables.yN[1] = last_estimatedPose.y;
  acadoVariables.yN[2] = last_estimatedPose.z;
  acadoVariables.yN[3] = last_estimatedPose.yaw;
  acadoVariables.yN[4] = 0.0;
  acadoVariables.yN[5] = 0.0;
  acadoVariables.yN[6] = 0.0;
  acadoVariables.yN[7] = 0.0;

  for (i = 0; i < NX; ++i)
    acadoVariables.x0[ i ] = acadoVariables.x[ i ];



//  std::cout<<acadoVariables.W<<std::endl
//           <<acadoVariables.WN<<std::endl
//           <<acadoVariables.u<<std::endl
//           <<acadoVariables.x<<std::endl
//           <<acadoVariables.x0<<std::endl
//           <<acadoVariables.y<<std::endl
//           <<acadoVariables.yN<<std::endl
//           <<acadoVariables.W[0*NY + 0]<<std::endl
//           <<acadoVariables.W[1*NY + 1]<<std::endl
//           <<acadoVariables.W[2*NY + 2]<<std::endl
//           <<acadoVariables.W[3*NY + 3]<<std::endl
//           <<acadoVariables.W[4*NY + 4]<<std::endl
//           <<acadoVariables.W[5*NY + 5]<<std::endl
//           <<acadoVariables.W[6*NY + 6]<<std::endl
//           <<acadoVariables.W[7*NY + 7]<<std::endl
//           <<acadoVariables.W[8*NY + 8]<<std::endl
//           <<acadoVariables.W[9*NY + 9]<<std::endl
//           <<acadoVariables.W[10*NY + 10]<<std::endl
//           <<acadoVariables.W[11*NY + 11]<<std::endl
//           <<acadoVariables.W[12*NY + 12]<<std::endl
//           <<acadoVariables.W[13*NY + 13]<<std::endl;


  acado_preparationStep();


  std::cout << "Constructor: MPC...Exit" << std::endl;

}

//ownStart()
void MPCProcess::ownStart()
{

  //// Topics ///
  // Subscribers
  dronePositionRefSub     = n.subscribe(dronePositionRefTopicName, 1, &MPCProcess::dronePositionRefsSubCallback, this);
  droneSpeedsRefSub       = n.subscribe(droneSpeedsRefTopicName,    1, &MPCProcess::droneSpeedsRefsSubCallback, this);
  droneTrajectoryAbsRefSub= n.subscribe(droneTrajectoryAbsRefTopicName,   1, &MPCProcess::droneTrajectoryAbsRefCommandCallback, this);
  droneTrajectoryRelRefSub= n.subscribe(droneTrajectoryRelRefTopicName,   1, &MPCProcess::droneTrajectoryRelRefCommandCallback, this);
  droneYawRefCommandSub   = n.subscribe(droneYawRefCommandTopicName, 1, &MPCProcess::droneYawRefCommandCallback, this);
  droneEstimatedPoseSubs   = n.subscribe(droneEstimatedPoseTopicName,      1, &MPCProcess::droneEstimatedPoseCallback, this);
  droneEstimatedSpeedsSubs = n.subscribe(droneEstimatedSpeedsTopicName,    1, &MPCProcess::droneEstimatedSpeedsCallback, this);
  droneControlModeSubs = n.subscribe(controlModeTopicName, 1, &MPCProcess::droneControlModeCallback,this);
  // Publishers
  drone_command_pitch_roll_publisher = n.advertise<droneMsgsROS::dronePitchRollCmd>(drone_command_pitch_roll_topic_name, 1);
  drone_command_dyaw_publisher      = n.advertise<droneMsgsROS::droneDYawCmd>(drone_command_dyaw_topic_name, 1);
  drone_command_daltitude_publisher = n.advertise<droneMsgsROS::droneDAltitudeCmd>(drone_command_daltitude_topic_name, 1);

  drone_position_reference_publisher   = n.advertise<droneMsgsROS::dronePose>(drone_position_reference_topic_name, 1);
  drone_yaw_reference_publisher = n.advertise<droneMsgsROS::droneYawRefCommand>(droneYawRefCommandTopicName,1);
  drone_speed_reference_publisher      = n.advertise<droneMsgsROS::droneSpeeds>(drone_speed_reference_topic_name, 1);
  drone_trajectory_reference_publisher = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(drone_trajectory_reference_topic_name, 1);


}

//ownStop()
void MPCProcess::ownStop()
{

}

//ownRun()
void MPCProcess::ownRun()
{
  setReference();
  setFeedback();

  acado_preparationStep();

  /* Perform the feedback step. */
 int acado_status = acado_feedbackStep();

// std::cout<<acado_status<<std::endl;

  /* Prepare for the next step. */
  getOutput();

}

void MPCProcess::setFeedback()
{
  acadoVariables.x0[0] = last_estimatedPose.x;
  acadoVariables.x0[1] = last_estimatedPose.y;
  acadoVariables.x0[2] = last_estimatedPose.z;
  acadoVariables.x0[3] = last_estimatedPose.yaw;
  acadoVariables.x0[4] = last_estimatedSpeed.dx;
  acadoVariables.x0[5] = last_estimatedSpeed.dy;
  acadoVariables.x0[6] = -last_estimatedSpeed.dz;
  acadoVariables.x0[7] = -last_estimatedSpeed.dyaw;

}
void MPCProcess::setReference()
{

  // Adapting Yaw not to change sign of turn at end of range [-PI, PI]

      error_yaw = current_yaw_reference.yaw - last_estimatedPose.yaw;
      ref_yaw = current_yaw_reference.yaw;

      if (error_yaw  > M_PI){
          ref_yaw -= 2 * M_PI;
      }
      else if(error_yaw < -M_PI){
          ref_yaw += 2 * M_PI;
      }
  if (control_mode.command == Controller_MidLevel_controlMode::TRAJECTORY_CONTROL){

      number_point = trajectory_command.droneTrajectory.size();
      if (iterator<number_point){
//          start_ = ros::WallTime::now();
          v_xy = 0.0;
          v_z = 0.0;

          dist_xy = 0.0;
          dist_z = 0.0;
          sgndist_z = 0.0;

          thr_xy = 0.40 + (vmax_xy-1.0)*0.2 ;
          thr_z = 0.20 + (vmax_z-0.4)*0.5;

          point1 << last_estimatedPose.x, last_estimatedPose.y, last_estimatedPose.z; //EKFPoint;
          point2 << trajectory_command.droneTrajectory[iterator].x,trajectory_command.droneTrajectory[iterator].y,trajectory_command.droneTrajectory[iterator].z;
//          std::cout<<"iterator "<<iterator<<std::endl
//                   <<"punto 1 "<<point1(0)<<"-"<<point1(1)<<"-"<<point1(2)<<std::endl
//                   <<"punto 2 "<<point2(0)<<"-"<<point2(1)<<"-"<<point2(2)<<std::endl
//                   <<"---------"<<std::endl;
          iter_point = 0;
          iter_move = 1;
          endpoint = false;
          last_dist = 0.0;
          it = -1;
          point_actual = point1;

          for (i = 0; i < N; ++i){
                if (endpoint){
                    acadoVariables.y[ i * NY + 0 ] = point1(0);
                    acadoVariables.y[ i * NY + 1 ] = point1(1);
                    acadoVariables.y[ i * NY + 2 ] = point1(2);
                    acadoVariables.y[ i * NY + 3 ] = ref_yaw;
                    acadoVariables.y[ i * NY + 4 ] = 0.0;
                    acadoVariables.y[ i * NY + 5 ] = 0.0;
                    acadoVariables.y[ i * NY + 6 ] = 0.0;
                    acadoVariables.y[ i * NY + 7 ] = 0.0;
                    acadoVariables.y[ i * NY + 8 ] = 0.0;
                    acadoVariables.y[ i * NY + 9 ] = 0.0;
                    acadoVariables.y[ i * NY + 10 ] = 0.0;
                    acadoVariables.y[ i * NY + 11 ] = 0.0;
                    acadoVariables.y[ i * NY + 12 ] = 0.0;
                    acadoVariables.y[ i * NY + 13 ] = 0.0;
                    acadoVariables.y[ i * NY + 14 ] = 0.0;
                    acadoVariables.y[ i * NY + 15 ] = 0.0;
//                    std::cout<<"New Point: "<< point1 <<std::endl
//                             <<"           "<<std::endl
//                             <<"           "<<std::endl
//                             <<"___________"<<std::endl;
                    continue;
                  }
                dist_xy = sqrt(pow((point2(0)-point1(0)),2.0) + pow((point2(1)-point1(1)),2.0));
                dist_z = abs(point2(2) - point1(2));
                sgndist_z = point2(2) - point1(2);
                t = max(dist_xy/vmax_xy , dist_z/vmax_z);
                v_xy = dist_xy/t;
                v_z = sgndist_z/t;

                if (dist_xy<thr_xy && dist_z<thr_z){
                    acadoVariables.y[ i * NY + 0 ] = point2(0);
                    acadoVariables.y[ i * NY + 1 ] = point2(1);
                    acadoVariables.y[ i * NY + 2 ] = point2(2);
                    acadoVariables.y[ i * NY + 3 ] = ref_yaw;
                    acadoVariables.y[ i * NY + 4 ] = (point2(0)-point1(0))/dist_xy*v_xy;
                    acadoVariables.y[ i * NY + 5 ] = (point2(1)-point1(1))/dist_xy*v_xy;
                    acadoVariables.y[ i * NY + 6 ] = v_z;
                    acadoVariables.y[ i * NY + 7 ] = 0.0;
                    acadoVariables.y[ i * NY + 8 ] = 0.0;
                    acadoVariables.y[ i * NY + 9 ] = 0.0;
                    acadoVariables.y[ i * NY + 10 ] = 0.0;
                    acadoVariables.y[ i * NY + 11 ] = 0.0;
                    acadoVariables.y[ i * NY + 12 ] = 0.0;
                    acadoVariables.y[ i * NY + 13 ] = 0.0;
                    acadoVariables.y[ i * NY + 14 ] = 0.0;
                    acadoVariables.y[ i * NY + 15 ] = 0.0;
//                    std::cout<<"New Point: "<< point1 <<std::endl
//                             <<"           "<<std::endl
//                             <<"           "<<std::endl
//                             <<"___________"<<std::endl;

                    dist = sqrt(pow((point2(0)-point_actual(0)),2.0) + pow((point2(1)-point_actual(1)),2.0));
                    if (last_dist > dist){
                        it = i;
//                        std::cout<<it<<std::endl;
                        break;
                      }
                    last_dist = dist;
                    if (iter_point < 1){
                      iterator++;
                      iter_point = 0;
//                      std::cout<<"iterator "<<iterator<<std::endl;
                      iter_move=1;
                    }else{
                      iter_point++;
                      iter_move=1;

                      }

                    point1 = point2;
                    if (iterator+iter_point < number_point){
                      point2 << trajectory_command.droneTrajectory[iterator+iter_point].x,
                                trajectory_command.droneTrajectory[iterator+iter_point].y,
                                trajectory_command.droneTrajectory[iterator+iter_point].z;
                    }else{
                      endpoint = true;
                    }


                    continue;
                  }

                  point << point1(0) + dt*iter_move*(point2(0)-point1(0))/dist_xy*v_xy,
                           point1(1) + dt*iter_move*(point2(1)-point1(1))/dist_xy*v_xy,
                           point1(2) + dt*iter_move*v_z;
                  acadoVariables.y[ i * NY + 0 ] = point(0);
                  acadoVariables.y[ i * NY + 1 ] = point(1);
                  acadoVariables.y[ i * NY + 2 ] = point(2);
                  acadoVariables.y[ i * NY + 3 ] = ref_yaw;
                  acadoVariables.y[ i * NY + 4 ] = (point2(0)-point1(0))/dist_xy*v_xy;
                  acadoVariables.y[ i * NY + 5 ] = (point2(1)-point1(1))/dist_xy*v_xy;
                  acadoVariables.y[ i * NY + 6 ] = v_z;
                  acadoVariables.y[ i * NY + 7 ] = 0.0;
                  acadoVariables.y[ i * NY + 8 ] = 0.0;
                  acadoVariables.y[ i * NY + 9 ] = 0.0;
                  acadoVariables.y[ i * NY + 10 ] = 0.0;
                  acadoVariables.y[ i * NY + 11 ] = 0.0;
                  acadoVariables.y[ i * NY + 12 ] = 0.0;
                  acadoVariables.y[ i * NY + 13 ] = 0.0;
                  acadoVariables.y[ i * NY + 14 ] = 0.0;
                  acadoVariables.y[ i * NY + 15 ] = 0.0;

                  dist = sqrt(pow((point(0)-point_actual(0)),2.0) + pow((point(1)-point_actual(1)),2.0));
                  if (last_dist > dist){
                      it = i;
//                      std::cout<<it<<std::endl;
                      break;
                    }
                  last_dist = dist;
//                  std::cout<<"New Point: "<< point <<std::endl
//                           <<"           "<<std::endl
//                           <<"           "<<std::endl
//                           <<"___________"<<std::endl;

                  dist_xy = sqrt(pow((point2(0)-point(0)),2.0) + pow((point2(1)-point(1)),2.0));
                  dist_z = abs(point2(2) - point(2));
//                  std::cout<<"Dist : " <<dist_xy<<" "<<dist_z<<" "<< sgndist_z<<std::endl
//                           <<"Thrs : " <<thr_xy<<" "<<thr_z<<std::endl
//                           <<"Temp : " << t <<std::endl
//                           <<"Vel : "<<v_xy<<" "<<v_z<<std::endl
//                           <<"__________"<<std::endl;
                  if (dist_xy > thr_xy || dist_z > thr_z){
                    iter_move++;
                    continue;
                    }
                  else{
                      iter_move = 1;
                      iter_point++;
//                      std::cout<<"iter_point "<<iter_point<<std::endl;

                      point1 = point2;
                      if (iterator+iter_point < number_point)
                        point2 << trajectory_command.droneTrajectory[iterator+iter_point].x,
                                  trajectory_command.droneTrajectory[iterator+iter_point].y,
                                  trajectory_command.droneTrajectory[iterator+iter_point].z;
                      else
                        endpoint = true;
                    }

            }

          if (it != -1){ // previous for loop broke
              // Set max distance ref as last ref points not to block on trajectory
              for (i = it; i < N; ++i){
                  acadoVariables.y[ i * NY + 0 ] = acadoVariables.y[ it * NY + 0 ];
                  acadoVariables.y[ i * NY + 1 ] = acadoVariables.y[ it * NY + 1 ];
                  acadoVariables.y[ i * NY + 2 ] = acadoVariables.y[ it * NY + 2 ];
                  acadoVariables.y[ i * NY + 3 ] = acadoVariables.y[ it * NY + 3 ];
                  acadoVariables.y[ i * NY + 4 ] = acadoVariables.y[ it * NY + 4 ];
                  acadoVariables.y[ i * NY + 5 ] = acadoVariables.y[ it * NY + 5 ];
                  acadoVariables.y[ i * NY + 6 ] = acadoVariables.y[ it * NY + 6 ];
                  acadoVariables.y[ i * NY + 7 ] = 0.0;
                  acadoVariables.y[ i * NY + 8 ] = 0.0;
                  acadoVariables.y[ i * NY + 9 ] = 0.0;
                  acadoVariables.y[ i * NY + 10 ] = 0.0;
                  acadoVariables.y[ i * NY + 11 ] = 0.0;
                  acadoVariables.y[ i * NY + 12 ] = 0.0;
                  acadoVariables.y[ i * NY + 13 ] = 0.0;
                  acadoVariables.y[ i * NY + 14 ] = 0.0;
                  acadoVariables.y[ i * NY + 15 ] = 0.0;

                }
            }

//            std::cout<<"First Speed: "<< acadoVariables.y[ 4+NY ]<<std::endl
//                     <<"            "<< acadoVariables.y[ 5+NY ]<<std::endl
//                     <<"            "<< acadoVariables.y[ 6+NY ]<<std::endl
//                     <<"____________"<<std::endl;

//            std::cout<<"Last Point: "<< acadoVariables.y[ 0+NY*19 ]<<std::endl
//                     <<"            "<< acadoVariables.y[ 1+NY*19 ]<<std::endl
//                     <<"            "<< acadoVariables.y[ 2+NY*19 ]<<std::endl
//                     <<"____________"<<std::endl;
//          end_ = ros::WallTime::now();
        }else{

          for (i = 0; i < N; ++i)
            {
              acadoVariables.y[ i * NY + 0 ] = trajectory_command.droneTrajectory[number_point].x;
              acadoVariables.y[ i * NY + 1 ] = trajectory_command.droneTrajectory[number_point].y;
              acadoVariables.y[ i * NY + 2 ] = trajectory_command.droneTrajectory[number_point].y;
              acadoVariables.y[ i * NY + 3 ] = ref_yaw;
              acadoVariables.y[ i * NY + 4 ] = 0.0;
              acadoVariables.y[ i * NY + 5 ] = 0.0;
              acadoVariables.y[ i * NY + 6 ] = 0.0;
              acadoVariables.y[ i * NY + 7 ] = 0.0;
              acadoVariables.y[ i * NY + 8 ] = 0.0;
              acadoVariables.y[ i * NY + 9 ] = 0.0;
              acadoVariables.y[ i * NY + 10 ] = 0.0;
              acadoVariables.y[ i * NY + 11 ] = 0.0;
              acadoVariables.y[ i * NY + 12 ] = 0.0;
              acadoVariables.y[ i * NY + 13 ] = 0.0;
              acadoVariables.y[ i * NY + 14 ] = 0.0;
              acadoVariables.y[ i * NY + 15 ] = 0.0;
          }

        }


//      double execution_time = (end_ - start_).toNSec() * 1e-6;
//      ROS_INFO_STREAM("Exectution time (ms): " << execution_time);
    }else {


      if (control_mode.command == Controller_MidLevel_controlMode::POSITION_CONTROL){
          for (i = 0; i < N; ++i)
            {
              acadoVariables.y[ i * NY + 0 ] = current_drone_position_reference.x;
              acadoVariables.y[ i * NY + 1 ] = current_drone_position_reference.y;
              acadoVariables.y[ i * NY + 2 ] = current_drone_position_reference.z;
              acadoVariables.y[ i * NY + 3 ] = ref_yaw;
              acadoVariables.y[ i * NY + 4 ] = 0.0;
              acadoVariables.y[ i * NY + 5 ] = 0.0;
              acadoVariables.y[ i * NY + 6 ] = 0.0;
              acadoVariables.y[ i * NY + 7 ] = 0.0;
              acadoVariables.y[ i * NY + 8 ] = 0.0;
              acadoVariables.y[ i * NY + 9 ] = 0.0;
              acadoVariables.y[ i * NY + 10 ] = 0.0;
              acadoVariables.y[ i * NY + 11 ] = 0.0;
              acadoVariables.y[ i * NY + 12 ] = 0.0;
              acadoVariables.y[ i * NY + 13 ] = 0.0;
              acadoVariables.y[ i * NY + 14 ] = 0.0;
              acadoVariables.y[ i * NY + 15 ] = 0.0;
          }

        }else{ //Speed

          for (i = 0; i < N; ++i)
            {
              acadoVariables.y[ i * NY + 0 ] = current_drone_position_reference.x;
              acadoVariables.y[ i * NY + 1 ] = current_drone_position_reference.y;
              acadoVariables.y[ i * NY + 2 ] = current_drone_position_reference.z;
              acadoVariables.y[ i * NY + 3 ] = ref_yaw;
              acadoVariables.y[ i * NY + 4 ] = current_drone_speed_reference.dx;
              acadoVariables.y[ i * NY + 5 ] = current_drone_speed_reference.dy;
              acadoVariables.y[ i * NY + 6 ] = current_drone_speed_reference.dz;
              acadoVariables.y[ i * NY + 7 ] = current_drone_speed_reference.dyaw;
              acadoVariables.y[ i * NY + 8 ] = 0.0;
              acadoVariables.y[ i * NY + 9 ] = 0.0;
              acadoVariables.y[ i * NY + 10 ] = 0.0;
              acadoVariables.y[ i * NY + 11 ] = 0.0;
              acadoVariables.y[ i * NY + 12 ] = 0.0;
              acadoVariables.y[ i * NY + 13 ] = 0.0;
              acadoVariables.y[ i * NY + 14 ] = 0.0;
              acadoVariables.y[ i * NY + 15 ] = 0.0;
          }
        }




    }
  acadoVariables.yN[ 0 ] = acadoVariables.y[ (N-1) * NY + 0 ];
  acadoVariables.yN[ 1 ] = acadoVariables.y[ (N-1) * NY + 1 ];
  acadoVariables.yN[ 2 ] = acadoVariables.y[ (N-1) * NY + 2 ];
  acadoVariables.yN[ 3 ] = acadoVariables.y[ (N-1) * NY + 3 ];
  acadoVariables.yN[ 4 ] = acadoVariables.y[ (N-1) * NY + 4 ];
  acadoVariables.yN[ 5 ] = acadoVariables.y[ (N-1) * NY + 5 ];
  acadoVariables.yN[ 6 ] = acadoVariables.y[ (N-1) * NY + 6 ];
  acadoVariables.yN[ 7 ] = acadoVariables.y[ (N-1) * NY + 7 ];

//  std::cout<<"Last Point: "<< acadoVariables.yN[ 0 ]<<std::endl
//           <<"            "<< acadoVariables.yN[ 1 ]<<std::endl
//           <<"            "<< acadoVariables.yN[ 2 ]<<std::endl
//           <<"____________"<<std::endl;

}

void MPCProcess::getOutput()
{
  // Setting the msgs
  drone_command_pitch_roll_msg.header.stamp = ros::Time::now();
  drone_command_dyaw_msg.header.stamp       = ros::Time::now();
  drone_command_daltitude_msg.header.stamp  = ros::Time::now();

  drone_command_pitch_roll_msg.pitchCmd = acadoVariables.x[2*NX-4]; // "Next iteration" of cmd
  drone_command_pitch_roll_msg.rollCmd = acadoVariables.x[2*NX-3];
  drone_command_dyaw_msg.dYawCmd = acadoVariables.x[2*NX-2];
  drone_command_daltitude_msg.dAltitudeCmd = acadoVariables.x[2*NX-1];

  // Publish
  if (std::isfinite(drone_command_pitch_roll_msg.pitchCmd)==1 && std::isfinite(drone_command_pitch_roll_msg.rollCmd)==1 && std::isfinite(drone_command_dyaw_msg.dYawCmd)==1 && std::isfinite(drone_command_daltitude_msg.dAltitudeCmd)==1 )
    {
      drone_command_pitch_roll_publisher.publish(drone_command_pitch_roll_msg);
      drone_command_dyaw_publisher.publish(drone_command_dyaw_msg);
      drone_command_daltitude_publisher.publish(drone_command_daltitude_msg);
    }else {
      drone_command_pitch_roll_msg.pitchCmd = 0.0;
      drone_command_pitch_roll_msg.rollCmd = 0.0;
      drone_command_dyaw_msg.dYawCmd = 0.0;
      drone_command_daltitude_msg.dAltitudeCmd = 0.0;

      drone_command_pitch_roll_publisher.publish(drone_command_pitch_roll_msg);
      drone_command_dyaw_publisher.publish(drone_command_dyaw_msg);
      drone_command_daltitude_publisher.publish(drone_command_daltitude_msg);
    }

}


void MPCProcess::setModel()
{

  //Set Limits

  for (i = 0; i < N; ++i){
    // Uper Limits
    acadoVariables.ubAValues[i * 9 + 0] = 1.0000000000000000e+12; // z_max
    acadoVariables.ubAValues[i * 9 + 1] = sat_pr; // sat_pr (vx)
    acadoVariables.ubAValues[i * 9 + 2] = sat_pr; // sat_pr (vy)
    acadoVariables.ubAValues[i * 9 + 3] = sat_dz; // sat_dz (vz)
    acadoVariables.ubAValues[i * 9 + 4] = sat_dyaw; // sat_dy (dyaw)
    acadoVariables.ubAValues[i * 9 + 5] = sat_cmd_pr; // roll_pitch_max (pitch_b)
    acadoVariables.ubAValues[i * 9 + 6] = sat_cmd_pr; // roll_pitch_max (roll_b)
    acadoVariables.ubAValues[i * 9 + 7] = sat_cmd_dz; // dyaw_max (dyaw_cmd)
    acadoVariables.ubAValues[i * 9 + 8] = sat_cmd_dyaw; // dz_max (dz)
    // Lower Limits
    acadoVariables.lbAValues[i * 9 + 0] = z_min; // z_min
    acadoVariables.lbAValues[i * 9 + 1] = -sat_pr; // sat_pr (vx)
    acadoVariables.lbAValues[i * 9 + 2] = -sat_pr; // sat_pr (vy)
    acadoVariables.lbAValues[i * 9 + 3] = -sat_dz; // sat_dz (vz)
    acadoVariables.lbAValues[i * 9 + 4] = -sat_dyaw; // sat_dy (dyaw)
    acadoVariables.lbAValues[i * 9 + 5] = -sat_cmd_pr; // roll_pitch_max (pitch_b)
    acadoVariables.lbAValues[i * 9 + 6] = -sat_cmd_pr; // roll_pitch_max (roll_b)
    acadoVariables.lbAValues[i * 9 + 7] = -sat_cmd_dz; // dyaw_max (dyaw_cmd)
    acadoVariables.lbAValues[i * 9 + 8] = -sat_cmd_dyaw; // dz_max (dz)

    }

  // Set Model Parameters

  for (i = 0; i < (N + 1); ++i){

    acadoVariables.od[i * NOD + 0] = A_pr;
    acadoVariables.od[i * NOD + 1] = t_r_pr;
    acadoVariables.od[i * NOD + 2] = delay_pr;
    acadoVariables.od[i * NOD + 3] = A_dz;
    acadoVariables.od[i * NOD + 4] = t_r_dz;
    acadoVariables.od[i * NOD + 5] = delay_dz;
    acadoVariables.od[i * NOD + 6] = A_dyaw;
    acadoVariables.od[i * NOD + 7] = t_r_dyaw;
    acadoVariables.od[i * NOD + 8] = delay_dyaw;

  }

}


void MPCProcess::setCost()
{
  if (control_mode.command == Controller_MidLevel_controlMode::SPEED_CONTROL){

           acadoVariables.W[0*NY + 0] = 1e-8;   // x
           acadoVariables.W[1*NY + 1] = 1e-8;   // y
           acadoVariables.W[2*NY + 2] = 1e-8;     // z
           acadoVariables.W[3*NY + 3] = q_yaw;   // yaw
           acadoVariables.W[4*NY + 4] = q_vel;   // vx
           acadoVariables.W[5*NY + 5] = q_vel;   // vy
           acadoVariables.W[6*NY + 6] = q_vz;    // vz
           acadoVariables.W[7*NY + 7] = q_dyaw;  // dyaw
           acadoVariables.W[8*NY + 8] = r_pr;   // pitch_b
           acadoVariables.W[9*NY + 9] = r_pr;   // roll_b
           acadoVariables.W[10*NY + 10] = r_dyaw; // dyaw_cmd
           acadoVariables.W[11*NY + 11] = r_dz;   // dz
           acadoVariables.W[12*NY + 12] = 1.0; // dpitch_b
           acadoVariables.W[13*NY + 13] = 1.0;   // droll_b
           acadoVariables.W[14*NY + 14] = 1.0; // ddyaw_cmd
           acadoVariables.W[15*NY + 15] = 1.0;   // ddz


     }else{
      if (control_mode.command == Controller_MidLevel_controlMode::TRAJECTORY_CONTROL){
          acadoVariables.W[0*NY + 0] = q_pos;   // x
          acadoVariables.W[1*NY + 1] = q_pos;   // y
          acadoVariables.W[2*NY + 2] = q_z;     // z
          acadoVariables.W[3*NY + 3] = q_yaw;   // yaw
          acadoVariables.W[4*NY + 4] = q_vel/10.0;   // vx
          acadoVariables.W[5*NY + 5] = q_vel/10.0;   // vy
          acadoVariables.W[6*NY + 6] = q_vz;    // vz
          acadoVariables.W[7*NY + 7] = q_dyaw;  // dyaw
          acadoVariables.W[8*NY + 8] = r_pr;   // pitch_b
          acadoVariables.W[9*NY + 9] = r_pr;   // roll_b
          acadoVariables.W[10*NY + 10] = r_dyaw; // dyaw_cmd
          acadoVariables.W[11*NY + 11] = r_dz;   // dz
          acadoVariables.W[12*NY + 12] = 1.0; // dpitch_b
          acadoVariables.W[13*NY + 13] = 1.0;   // droll_b
          acadoVariables.W[14*NY + 14] = 1.0; // ddyaw_cmd
          acadoVariables.W[15*NY + 15] = 1.0;   // ddz


        }else{

      acadoVariables.W[0*NY + 0] = q_pos;   // x
      acadoVariables.W[1*NY + 1] = q_pos;   // y
      acadoVariables.W[2*NY + 2] = q_z;     // z
      acadoVariables.W[3*NY + 3] = q_yaw;   // yaw
      acadoVariables.W[4*NY + 4] = q_vel;   // vx
      acadoVariables.W[5*NY + 5] = q_vel;   // vy
      acadoVariables.W[6*NY + 6] = q_vz;    // vz
      acadoVariables.W[7*NY + 7] = q_dyaw;  // dyaw
      acadoVariables.W[8*NY + 8] = r_pr;   // pitch_b
      acadoVariables.W[9*NY + 9] = r_pr;   // roll_b
      acadoVariables.W[10*NY + 10] = r_dyaw; // dyaw_cmd
      acadoVariables.W[11*NY + 11] = r_dz;   // dz
      acadoVariables.W[12*NY + 12] = 1.0; // dpitch_b
      acadoVariables.W[13*NY + 13] = 1.0;   // droll_b
      acadoVariables.W[14*NY + 14] = 1.0; // ddyaw_cmd
      acadoVariables.W[15*NY + 15] = 1.0;   // ddz
        }

    }

  acadoVariables.WN[0*NYN + 0] = acadoVariables.W[0*NY + 0];   // x
  acadoVariables.WN[1*NYN + 1] = acadoVariables.W[1*NY + 1];   // y
  acadoVariables.WN[2*NYN + 2] = acadoVariables.W[2*NY + 2];   // z
  acadoVariables.WN[3*NYN + 3] = acadoVariables.W[3*NY + 3];   // yaw
  acadoVariables.WN[4*NYN + 4] = acadoVariables.W[4*NY + 4];   // vx
  acadoVariables.WN[5*NYN + 5] = acadoVariables.W[5*NY + 5];   // vy
  acadoVariables.WN[6*NYN + 6] = acadoVariables.W[6*NY + 6];   // vz
  acadoVariables.WN[7*NYN + 7] = acadoVariables.W[7*NY + 7];   // dyaw

  acado_initializeSolver();

}


void MPCProcess::dronePositionRefsSubCallback(const droneMsgsROS::dronePositionRefCommandStamped::ConstPtr &msg) {
  current_drone_position_reference = (msg->position_command);
}

void MPCProcess::droneSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg) {
  current_drone_speed_reference  = (*msg);
}

void MPCProcess::droneControlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode::ConstPtr& msg){

  if (control_mode.command != (msg->command)){
        control_mode = *msg;
        setCost();
        iterator = 0;
    }
}

void MPCProcess::droneTrajectoryAbsRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg) {
  trajectory_command = (*msg);
  int n = trajectory_command.droneTrajectory.size();

  if(n==0 && (!stay_in_position) ) {
      droneMsgsROS::dronePositionRefCommand actual_position;
      actual_position.x = +last_estimatedPose.x;
      actual_position.y = +last_estimatedPose.y;
      actual_position.z = +last_estimatedPose.z;
      trajectory_command.droneTrajectory.push_back(actual_position);
      trajectory_command.is_periodic        = false;
      trajectory_command.initial_checkpoint = 0;
      n = 1;
      stay_in_position = true;
  } else {
      if ( n>0 ) {
          stay_in_position = false;
      } else {
          return;
      }
  }

  return;
}

void MPCProcess::droneTrajectoryRelRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg)
{

    rel_trajectory_command = (*msg);
    trajectory_command = (*msg);

    int n = rel_trajectory_command.droneTrajectory.size();
    if(n==0 && (!stay_in_position) ) {
        droneMsgsROS::dronePositionRefCommand actual_position;
        actual_position.x = 0.0;
        actual_position.y = 0.0;
        actual_position.z = 0.0;
        rel_trajectory_command.droneTrajectory.push_back(actual_position);
        rel_trajectory_command.is_periodic        = false;
        rel_trajectory_command.initial_checkpoint = 0;
        n = 1;
        stay_in_position = true;
    } else {
        if ( n>0 ) {
            stay_in_position = false;
        } else {
            return;
        }
    }

    trajectory_command.droneTrajectory.clear();
    std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it_mod = (rel_trajectory_command.droneTrajectory).begin();
    for (std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it = (rel_trajectory_command.droneTrajectory).begin();
         it != (rel_trajectory_command.droneTrajectory).end();
         ++it) {

        droneMsgsROS::dronePositionRefCommand next_waypoint;
        next_waypoint.x = it_mod->x;
        next_waypoint.y = it_mod->y;
        next_waypoint.z = it_mod->z;
        next_waypoint.x += last_estimatedPose.x;
        next_waypoint.y += last_estimatedPose.y;
        next_waypoint.z += last_estimatedPose.z;

        trajectory_command.droneTrajectory.push_back(next_waypoint);
        std::cout<<next_waypoint<<std::endl;
    }

    return;
}

void MPCProcess::droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr &msg) {
    current_yaw_reference = (*msg) ;
}


void MPCProcess::droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg) {
    last_estimatedPose = (*msg);
}

void MPCProcess::droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg) {
    last_estimatedSpeed = (*msg);
}

double MPCProcess::max(double d1, double d2){
    if (d1 >= d2){
        return d1;
    }else{
        return d2;
    }
}
