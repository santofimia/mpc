//////////////////////////////////////////////////////
//  mpc_process_main.cpp
//
//  Created on: Nov 15, 2018
//      Author: psantofimia
//
//  Last modification on: Nov 15, 2018
//      Author: psantofimia
//
//////////////////////////////////////////////////////


#include "mpc_process.h"

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;

    MPCProcess my_mpc_controller;
    my_mpc_controller.setUp();
    my_mpc_controller.stop(); // The process will have to be started by a service call from another process

    ros::Rate r(10); //dt = 0.1

    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        //running the process
        my_mpc_controller.run();
        r.sleep();
    }

    return 0;


}
