# Model Predictive Control

## Getting the model

- **Simple Model**

In order to get a Simple Workable Model, we are going to assume that each input corresponds to one variable without any interaction between them. In Aerostack, controllers have four inputs, so we are going to estimate four SISO systems:

1. Pitch_Body --> Vx_Body
2. Roll_Body --> Vy_Body
3. dAltitude --> Vz
4. dYaw_Cmd --> dYaw

As an option, to estimate those models, Matlab is used. [Interactively Estimate Plant from Measured or Simulated Response Data](https://es.mathworks.com/help/slcontrol/ug/interactively-estimate-plant-from-measured-or-simulated-response-data.html). By now, each estimation corresponds to a One Pole+Delay plant.

Once a model is obtained, [ACADO Toolkit](http://acado.sourceforge.net/doc/pdf/acado_manual.pdf) is used to introduce it, changing it on *./model/mpc_solver_setup.cpp*.

- **Complex Model**

Carlos Santos is trying to estimate a new model as a *MIMO*, that is, with interactions between the different variables. By now, it is not implemented, but it is possible that soon it will be integrated into that repository.

- **Modify Model**

If the Model architecture is the same as the one is set (four *One Pole+Delay SISO*), it is not necessary to compile again the model, it would be just necessary to change parameters.

- **Tuning Weights**

Model is set to minimize an LSQ Cost function, so in order to change its behaviour, you can modify those parameters.

- **Use Modes**

As a classic controller, it works on 3 Modes:

1. **Position**
The UAV will move to the Reference Position(XYZ), with Reference Yaw.
2. **Speed**
The UAV will move at a Reference Speed(XYZ), with Reference Yaw.
3. **Trajectory**
The UAV will follow a path at a desired maximum speed. 
It has some problems in some trajectories, when the drone should go to a point, and come back near the actual one. In this case, sometimes the UAV will stop in a middle point (as minimize the cost function), in order to correct that problem, the reference points given to the solver (in the horizon time) will be the same beginning with the one that the distance to the actual point is less than the previous one. 

